var gulp = require("gulp"),
    sass = require("gulp-sass"),
    cssnano = require("gulp-cssnano"),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename"),
    slim = require("gulp-slim"),
    coffee = require('gulp-coffee');

gulp.task('text', function() {
  console.log('YoYo');
  
});

gulp.task('slim', function(){
    gulp.src("./src/*.slim")
      .pipe(slim({
        pretty: true
      }))
      .pipe(gulp.dest("./dist/"));
  });

gulp.task("sass", function() {
    return gulp.src("./src/assets/sass/*.sass")
        .pipe(concat('style.sass'))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
         }))
        .pipe(cssnano())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest("dist/css"));
});

gulp.task('coffee', function() {
  gulp.src('src/assets/js/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest('src/assets/js/'));
});

gulp.task("scripts", function() {
  return gulp.src("src/assets/js/*.js") // директория откуда брать исходники
      .pipe(concat('scripts.js')) // объеденим все js-файлы в один 
      .pipe(uglify()) // вызов плагина uglify - сжатие кода
      .pipe(rename({ suffix: '.min' })) // вызов плагина rename - переименование файла с приставкой .min
      .pipe(gulp.dest("dist/js")); // директория продакшена, т.е. куда сложить готовый файл
});

gulp.task('imgs', function() {
  return gulp.src("src/assets/img/*.+(jpg|jpeg|png|gif)")
      .pipe(imagemin({
          progressive: true,
          svgoPlugins: [{ removeViewBox: false }],
          interlaced: true
      }))
      .pipe(gulp.dest("dist/images"))
});

gulp.task("watch", function() {
  gulp.watch("src/*.slim", ["slim"]);
  gulp.watch("src/assets/js/*.coffee", ["coffee"]);
  gulp.watch("src/assets/js/*.js", ["scripts"]);
  gulp.watch("src/assets/sass/*.sass", ["sass"]);
  gulp.watch("src/assets/images/*.+(jpg|jpeg|png|gif)", ["imgs"]);
});

gulp.task("default", ["slim", "sass", "coffee", "scripts", "imgs", "watch"]);