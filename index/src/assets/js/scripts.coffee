selecting = ["Печень утки разварная с артишоками.", 
             "Головы щучьи с чесноком да свежайшая сёмгушка.", 
             "Филе из цыплят с трюфелями в бульоне"]
footer = '<span>Чего сидишь? Порадуй котэ, </span> <a class="buy" onclick="buy('
footer_2 = '); return false;" href="">купи.</a></div>'
cancel = '<span class="cancel">Котэ не одобряет?</span>'

$('.bcat__body').click( () ->
    #event.preventDefault()
    id = $(this)[0].dataset.id
    buy (id)
    )
#Обработчик покупки товара

buy = (yum) ->
    syum = "#"+yum
    if !$(syum).hasClass('empty')
        $(syum).toggleClass('selected')
        $('.bcat').mouseleave( () -> 
            hover (yum)
        )
        if $(syum).hasClass('selected')
            $(syum).find('.bcat__footer').html(selecting[yum])
        else
            $(syum).find('.bcat__footer').html(footer + yum + footer_2)
            $(syum).find('p.text').show()
            $(syum).find('p.cancel').hide()

#Показывает что товара нет. Функция принимает id товара (0..2)
no_yum = (yum) ->
    empty = ["Печалька, с фуа-гра закончился",
             "Печалька, с рыбой закончился",
             "Печалька, с курой закончился"]
    syum = "#"+yum
    $(syum).addClass('empty')
    $(syum).find('.bcat__footer').html(empty[yum])

hover = (yum) ->
    syum = "#"+yum
   
    $(syum).mouseenter( () ->
        if $(syum).hasClass('selected')
            $(syum).find('p.text').hide()
            $(syum).find('p.cancel').show()
    )
    $(syum).mouseleave( () ->
        if $(syum).hasClass('selected')
            $(syum).find('p.text').show()
            $(syum).find('p.cancel').hide()
    )