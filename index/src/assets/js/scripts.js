var buy, cancel, footer, footer_2, hover, no_yum, selecting;

selecting = ["Печень утки разварная с артишоками.", "Головы щучьи с чесноком да свежайшая сёмгушка.", "Филе из цыплят с трюфелями в бульоне"];

footer = '<span>Чего сидишь? Порадуй котэ, </span> <a class="buy" onclick="buy(';

footer_2 = '); return false;" href="">купи.</a></div>';

cancel = '<span class="cancel">Котэ не одобряет?</span>';

$('.bcat__body').click(function() {
  var id;
  //event.preventDefault()
  id = $(this)[0].dataset.id;
  return buy(id);
});

//Обработчик покупки товара
buy = function(yum) {
  var syum;
  syum = "#" + yum;
  if (!$(syum).hasClass('empty')) {
    $(syum).toggleClass('selected');
    $('.bcat').mouseleave(function() {
      return hover(yum);
    });
    if ($(syum).hasClass('selected')) {
      return $(syum).find('.bcat__footer').html(selecting[yum]);
    } else {
      $(syum).find('.bcat__footer').html(footer + yum + footer_2);
      $(syum).find('p.text').show();
      return $(syum).find('p.cancel').hide();
    }
  }
};

//Показывает что товара нет. Функция принимает id товара (0..2)
no_yum = function(yum) {
  var empty, syum;
  empty = ["Печалька, с фуа-гра закончился", "Печалька, с рыбой закончился", "Печалька, с курой закончился"];
  syum = "#" + yum;
  $(syum).addClass('empty');
  return $(syum).find('.bcat__footer').html(empty[yum]);
};

hover = function(yum) {
  var syum;
  syum = "#" + yum;
  $(syum).mouseenter(function() {
    if ($(syum).hasClass('selected')) {
      $(syum).find('p.text').hide();
      return $(syum).find('p.cancel').show();
    }
  });
  return $(syum).mouseleave(function() {
    if ($(syum).hasClass('selected')) {
      $(syum).find('p.text').show();
      return $(syum).find('p.cancel').hide();
    }
  });
};
